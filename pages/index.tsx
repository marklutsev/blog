import type { NextPage } from 'next'
import Head from 'next/head'
import Image from 'next/image'
import styles from '../styles/Home.module.sass'

const Home: NextPage = () => {
    return (
        <div className={styles.container}>
            <Head>
                <title>Blog | Home</title>
                <meta name="description" content="This is my personal blog" />
                <link rel="icon" href="/favicon.ico" />
            </Head>

            <main>
                
            </main>

            <footer>
            </footer>
        </div>
    )
}

export default Home
