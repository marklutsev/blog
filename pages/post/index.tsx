const Post = (props: any) => {
    console.log(props);
    return(<p>My Props Link Params {props.myParams.id}</p>);
}

Post.getInitialProps = ({query} : {query: any}) => {
    return {myParams: query}
}

export default Post;